#include <iostream>
using namespace std;

struct tree
{
	char *Eng_Cell;
	char *Rus_Cell;
	tree *left;
	tree *right;
}*proot;

// ���������� ������
class EngRus_Dictionary // ��� ������
{
private:
	char *eng, // ����� ���������� �����
		 *rus; // ����� ������� �����
	int Quantity = 0;

public:
	EngRus_Dictionary(char *eng_word, char *rus_word) // ����������� ������
	{
		set_the_Words(eng_word, rus_word); // ����� ������� ��������� ����
	}

	void set_the_Words(char *eng_word, char *rus_word) // ��������� ���� � ������� ��.��.��
	{
		//cout << eng_word << "   --------------" << endl;
		//cout << rus_word << "   --------------" << endl;
		eng = eng_word; // ������������� ����������� �����
		rus = rus_word; // ������������� �������� �����
	}

	/*������� ���-�� ���-������ ���� � ������*/void Words_Quantity_Verification(tree *p)
	{
	if (p == NULL) return;
	else if (p != NULL) Quantity++;
	Words_Quantity_Verification(p->left);
	Words_Quantity_Verification(p->right);
		//cout << p->inf << "\t";
	}

	/*������ ������ � ���������� � ���� �����*/void Creation_of_the_Massive()
	{
		cout << "Quantity: " << Quantity << endl;
		Quantity++;//� ������������ ��������� ���������� ����������
		//������ ��������� ������ ����������:
		char ***massive;
		massive = new char **[Quantity];
		for (int i = 0; i < Quantity; i++)
		{
			massive[i] = new char*[2];
		}
		//������� ����� �����, � ����� ������ ����� � ������
		massive[0][0] = eng; massive[0][1] = rus;//���������� � ������ ����� �����
		if (Quantity > 1)
		{
			int i = 1;
			Word_Recording_into_the_Massive(proot, massive, i);//�-��� ������ ���� �� ������ � ������
		}
		for (int i = 0; i < Quantity; i++)
		{
			cout << massive[i][0] << " - " << massive[i][1] << endl;
		}
		proot = AVL_Tree(0, Quantity - 1, massive);//������� � �������� ��������� ������ �� �������
		EnhanceWay(proot);
	}

	/*���������� ������� ������� ������� �� ������*/char ***Word_Recording_into_the_Massive(tree *p, char ***massive, int i)
	{
		if (p == NULL) return massive;
		Words_Quantity_Verification(p->left);
		if (p != NULL)
		{
			massive[i][0] = p->Eng_Cell;
			massive[i][1] = p->Rus_Cell;
			i++;
		};
		Words_Quantity_Verification(p->right);
		return massive;
	}

	/*������� �������� ����������������� ��������� ������*/tree *AVL_Tree(int L, int R, char ***m)
	{
		tree *ps;
		int i;
		if (L > R)return NULL; 
			i = (L + R) / 2; 
			ps = new tree; 
		//	ps->inf = m[i - 1]; 
			ps->Eng_Cell = m[i][0];
			ps->Rus_Cell = m[i][1];
			ps->left = AVL_Tree(L, i - 1, m); 
			ps->right = AVL_Tree(i + 1, R, m); 
			return ps; 
	}

	/*����� ������-�������*/void EnhanceWay(tree *p)
	{
	if (p == NULL) return; 
	EnhanceWay(p->left); 
	cout << "Dictionary: " << p->Eng_Cell << " - " << p->Rus_Cell << endl;;
	EnhanceWay(p->right); 
	}
};